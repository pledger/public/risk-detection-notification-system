from area import Area
import v2xlib.v2xcom as v2xcom
from awareness_service import AwarenessService
from own_prometheus_client import PrometheusClient
from bsc import BSC
from cam_service import CAMService
from ldm import LDM
from awareness_api import AwarenessAPI
from utils import *
from typing import List
from instances import Instances
import argparse
import threading
import logging
import random

class AwarenessApp:
    def __init__(self, prometheus_client: PrometheusClient, area_warning: Area, area_critical: Area, station_id: int, period: int, broker_host: str, broker_port: int, gps_host: str, gps_port: int, instances: List[str]):
        super().__init__()
        self.prometheus_client = prometheus_client
        self.ldm = LDM(area_warning, area_critical)
        self.connection = v2xcom.Connection(
            broker_host=broker_host, broker_port=broker_port, connection_name="awareness_app"+str(int(random.random()*10**12)))
        try:
            self.connection.connect()
        except:
            print("No connection to MQTT")
            exit(19)
        self.instances = Instances(instances=instances, connection=self.connection)
        #self.instance = v2xcom.Instance(instance_name, self.connection)
        self.awareness_service = AwarenessService(
            station_id, self.ldm, area_critical, area_warning, broker_host, broker_port, self.instances)
        self.bsc = BSC(self.ldm, station_id, gps_host, gps_port)
        self.cam_service = CAMService(
            period, station_id, broker_host, broker_port, self.instances, self.ldm, self.prometheus_client)
        self.api = AwarenessAPI(self.ldm)

    def start(self):
        thread_ldm = threading.Thread(target=self.ldm.start, daemon=True)
        thread_bsc = threading.Thread(target=self.bsc.start, daemon=True)
        thread_cam = threading.Thread(
            target=self.cam_service.start, daemon=True)
        thread_awareness = threading.Thread(
            target=self.awareness_service.start, daemon=True)
        thread_api = threading.Thread(target=self.api.start, daemon=True)

        print("starting threads")
        thread_ldm.start()
        thread_bsc.start()
        thread_cam.start()
        thread_awareness.start()
        thread_api.start()

        thread_ldm.join()
        thread_bsc.join()
        thread_cam.join()
        thread_awareness.join()
        thread_api.join()
        print("Ending app")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="First stage of what will be the CAM service: It only emits CAMs")
    parser.add_argument('--period', action='store', type=int, default=1000,
                        help='The period on which the CAM messages will be emitted')
    parser.add_argument('--station_id', dest='station_id', action='store',
                        type=int, default=1, help='The station ID used on CAMs')
    parser.add_argument('--mqtt_host', dest='mqtt_host', action='store',
                        type=str, default="localhost", help='The host of the MQTT broker')
    parser.add_argument('--mqtt_port', dest='mqtt_port', action='store',
                        type=int, default=1883, help='The port of the MQTT broker')
    parser.add_argument('--gps_host', dest='gps_host', action='store',
                        type=str, default="localhost", help='The host of the BSC-GPS')
    parser.add_argument('--gps_port', dest='gps_port', action='store',
                        type=int, default=2947, help='The host of the BSC-GPS')
    parser.add_argument('--instances', nargs="+", dest='instances', action='store',
                        default="test1", help='The name of the V2XCOM instances'+
                        ' (It has to be placed the following way: [--instances instance1 instance2 ...]')
    parser.add_argument('--delays_window', dest='delays_window', action='store',
                        type=int, default=50, help='The max number of packet delays used to compute the average delay')
    parser.add_argument('--time_window', dest='time_window', action='store',
                        type=int, default=2000, help='The time window (in milis) on which the packet loss will be computed (Must not be higher than 65536)')
    parser.add_argument('--frequency', dest='frequency', action='store',
                        type=int, default=10, help='The number of packets per second expected to receive from each node (Is used to compute the packet loss)')
    parser.add_argument('--debug', dest='debug', action='store_true',
                        default=False, help='Activate the log of debugging messages')
    args = parser.parse_args()

    area_critical = Area(area_type="circle", a=40, b=0, azimuth=0,
                         center_lat=41.407905, center_lon=2.204862)
    area_warning = Area(area_type="circle", a=100, b=0, azimuth=0,
                        center_lat=41.407905, center_lon=2.204862)

    logging.basicConfig(level=logging.INFO)
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    print("Starting the Awareness App")
    prometheus = PrometheusClient(delays_window=args.delays_window,
                                  time_window_milis=args.time_window, frequency=args.frequency)
    app = AwarenessApp(prometheus_client=prometheus,
                       area_warning=area_warning,
                       area_critical=area_critical,
                       station_id=args.station_id,
                       period=args.period,
                       broker_host=args.mqtt_host,
                       broker_port=args.mqtt_port,
                       gps_host=args.gps_host,
                       gps_port=args.gps_port,
                       instances=args.instances
                       )
    app.start()
