from typing import List, Callable
from v2xlib.v2xcom.instance import Instance
from v2xlib.v2xcom.connection import Connection
from v2xlib.v2xcom.socket import SocketConfig, Socket
from v2xlib.v2xcom.received_message import ReceivedMessage


class Sockets:
    def __init__(self, sockets : List[Socket]):
        self.sockets = sockets
    
    def __del__(self):
        for socket in self.sockets:
            socket.close()

    def send(self, payload: str):
        for sock in self.sockets:
            sock.send(payload=payload)
    
    def set_payload_cb(self, payload_cb):
        for sock in self.sockets:
            sock.set_payload_cb(payload_cb=payload_cb)

class Instances:
    def __init__(self, instances: List[str], connection: Connection):
        self.connection = connection
        self.instances = list()
        for inst in instances:
            self.instances.append(Instance(inst, self.connection))
        self.sockets = list()
    
    def open_socket(self, socket_config: SocketConfig, on_receive: Callable[[ReceivedMessage], None]):
        sockets_created = list()
        for instance in self.instances:
            sockets_created.append(instance.open_socket(socket_config=socket_config, on_receive_cb=on_receive))
        sockets_object = Sockets(sockets_created)
        self.sockets.append(sockets_object)
        return sockets_object
    
