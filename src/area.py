
class Area:
    def __init__(self, area_type: str, a: float, b: float, azimuth: float, center_lat: float, center_lon: float):
        super().__init__()
        self.area_type = area_type
        self.a = a
        self.b = b
        self.azimuth = azimuth
        self.center_lat = center_lat
        self.center_lon = center_lon
