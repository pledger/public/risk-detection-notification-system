from v2xlib.messages.cam import CoopAwareness
from v2xlib.messages.its_container import ReferencePosition
from utils import get_current_delta_time
from utils import position_in_area
from area import Area
import copy
import threading
import time
import json


class LDM:
    def __init__(self, warning_area: Area, critical_area: Area, output_file: str = "output.json"):
        self.elements = dict()
        self.lock = threading.Lock()
        self.output_file = output_file
        self.tram_in_critical = False
        self.tram_in_warning = False
        self.warning_area = warning_area
        self.critical_area = critical_area

    def new_position(self, station_id: int, position: CoopAwareness):
        self.lock.acquire()
        try:
            if not station_id in self.elements.keys():
                self.elements[station_id] = list()
            self.elements[station_id].append(position)
        finally:
            self.lock.release()

    def get_position(self, station_id: int) -> CoopAwareness:
        self.lock.acquire()
        try:
            if station_id in self.elements.keys() and len(self.elements[station_id]) > 0:
                return self.elements[station_id][-1]
        finally:
            self.lock.release()

    def get_positions(self):
        positions = list()
        self.lock.acquire()
        try:
            for station_id in self.elements.keys():
                positions.append(self.elements[station_id][-1])
        finally:
            self.lock.release()
            return positions

    def get_positions_station_id(self):
        positions = dict()
        self.lock.acquire()
        try:
            for station_id in self.elements.keys():
                positions[station_id] = self.elements[station_id][-1]
        finally:
            self.lock.release()
            return positions

    def get_reference_position(self, station_id: int) -> ReferencePosition:
        self.lock.acquire()
        try:
            if station_id in self.elements.keys():
                return self.elements[station_id][-1].camParameters.basicContainer.referencePosition
        finally:
            self.lock.release()

    def remove_old_entries(self):
        self.lock.acquire()
        try:
            for key, value in self.elements.items():
                for cam in value:
                    if cam.generationDeltaTime.__dict__() < get_current_delta_time()-120:
                        value.remove(cam)
        finally:
            self.lock.release()

    def set_tram_in_warning(self):
        self.tram_in_warning = True
        self.tram_in_critical = False

    def set_tram_in_critical(self):
        self.tram_in_warning = True
        self.tram_in_critical = True

    def set_tram_out_area(self):
        self.tram_in_warning = False
        self.tram_in_critical = False

    def start(self):
        i = 0
        to_write = list()
        while True:
            positions = self.get_positions_station_id()
            to_write_object = {
                "timestamp": int(time.time()),
                "tram": {
                    "latitude": 0.0,
                    "longitude": 0.0
                },
                "obus": []
            }
            for position_id in positions.keys():
                position = positions[position_id]
                if position.camParameters.basicContainer.stationType.value == 11:
                    to_write_object["tram"] = {
                        "station_id": position_id,
                        "latitude": position.camParameters.basicContainer.referencePosition.latitude.value /
                        (10**7),
                        "longitude": position.camParameters.basicContainer.referencePosition.longitude.value /
                        (10**7)
                    }
                else:
                    risk = "NORISK"
                    if self.tram_in_critical and position_in_area(
                        position.camParameters.basicContainer.referencePosition.latitude.value,
                        position.camParameters.basicContainer.referencePosition.longitude.value,
                        self.critical_area
                    ):
                        risk = "CRITICAL"
                    elif self.tram_in_warning and position_in_area(
                        position.camParameters.basicContainer.referencePosition.latitude.value,
                        position.camParameters.basicContainer.referencePosition.longitude.value,
                        self.warning_area
                    ):
                        risk = "WARNING"
                    obu_write = {
                        "station_id": position_id,
                        "position": {
                            "latitude": position.camParameters.basicContainer.referencePosition.latitude.value /
                            (10**7),
                            "longitude": position.camParameters.basicContainer.referencePosition.longitude.value /
                            (10**7)
                        },
                        "risk": risk
                    }

                    to_write_object["obus"].append(obu_write)
            to_write.append(to_write_object)
            if i > 29:
                self.remove_old_entries()
                out = open(self.output_file, 'a')
                json.dump(to_write, out, indent=4)
                out.close()
                to_write = []
                i = -1
            i = i+1
            time.sleep(1)
