from prometheus_client import Gauge
from v2xlib.messages.cam import CoopAwareness, CAM
import time
import logging

class PrometheusClient:
    def __init__(self,delays_window: int = 50, time_window_milis: int = 2000, frequency: int = 10):
        self.delays = list()
        self.delays_window = delays_window
        self.delays_gauge = Gauge('delay', 'Average registered delays for the last '+str(delays_window)+" packets")
        self.detected_stations_gauge = Gauge('stations', "Number of detected stations (Depending on this number other metrics can vary)")
        self.detected_stations = dict() # key -> station_id, value -> last time seen
        self.packet_loss = Gauge("packetloss", "Packet loss within the time window of "+str(time_window_milis)+" milis. Computed on expected messages for the detected stations")
        self.packets = list()
        self.frequency = frequency #In expected packets each second 
        self.time_window_milis = time_window_milis

    def new_cam(self, cam: CAM):
        logging.debug("PrometheusClient: new_cam - Starting function")
        self.add_delay(cam.cam)
        self.check_station(cam.header.stationID.value, cam.cam.generationDeltaTime.value)
        self.new_packet(cam.cam.generationDeltaTime.value)
        logging.debug("PrometheusClient: new_cam - Ending function")

    def check_station(self, station_id: int, gen_time: int):
        logging.debug("PrometheusClient: check_station - Starting function")
        self.detected_stations[station_id] = gen_time
        # Remove possible old stations
        logging.debug("PrometheusClient: check_station - Removing possible old stations")
        for station in list(self.detected_stations.keys()):
            if self.time_between_two_delta_times(self.detected_stations[station], self.get_current_delta_time())>self.time_window_milis:
                self.detected_stations.pop(station)
        # Update Gauge
        logging.debug("PrometheusClient: check_station - Updating Gauge")
        self.detected_stations_gauge.set(len(
            self.detected_stations
        ))
        logging.debug("PrometheusClient: check_station - Ending function")
    
    def new_packet(self, genTime: int):
        logging.debug("PrometheusClient: new_packet - Starting function")
        self.packets.append(genTime)
        current_time = self.get_current_delta_time()
        for packet_time in self.packets:
            if self.time_between_two_delta_times(packet_time, current_time)>self.time_window_milis:
                self.packets.remove(packet_time)
        time_window = max(self.packets)-min(self.packets)
        if time_window>self.time_window_milis:
            time_window = self.time_window_milis
        expected_packets = (time_window/1000)*self.frequency*len(self.detected_stations)
        if expected_packets>0:
            prr = len(self.packets)/expected_packets
            if prr>1:
                prr = 1
            self.packet_loss.set(1-prr)
        else:
            self.packet_loss.set(0)
        logging.debug("PrometheusClient: new_packet - Ending function")

    def add_delay(self, cam: CoopAwareness):
        logging.debug("PrometheusClient: add_delay - Starting function")
        sent_time = cam.generationDeltaTime.value
        received_time = self.get_current_delta_time()
        delay = self.time_between_two_delta_times(sent_time, received_time)
        if len(self.delays)>=self.delays_window:
            self.delays.pop(-1)
        self.delays.insert(0, delay)
        self.recompute_delays()
        logging.debug("PrometheusClient: add_delay - Ending function")
    
    def get_current_delta_time(self):
        return self.timestamp_to_delta_time(time.time())

    def time_between_two_delta_times(self, time1, time2):
        to_return = time2-time1
        if to_return<0:
            to_return = time2+65536-time1
        return to_return

    def timestamp_to_delta_time(self, time_seconds: float):
        time_milis = time_seconds*1000
        return (time_milis-1072915200000)%65536
    
    def recompute_delays(self):
        logging.debug("PrometheusClient: recompute_delays - Starting function")
        result = 0
        for elem in self.delays:
            result += elem
        result = result/len(self.delays)
        self.delays_gauge.set(result)
        logging.debug("PrometheusClient: recompute_delays - Ending function")
