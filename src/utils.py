import time
import geopy.distance
import math
from area import Area

def timestamp_to_delta_time(time: int):
    return (time - 1072911600) % 65536

def get_current_delta_time():
    return timestamp_to_delta_time(int(time.time()))

def lon_distance(lon, area):
    coords_1 = (0, lon)
    coords_2 = (0, area.center_lon)
    distance = geopy.distance.geodesic(coords_1, coords_2).m
    return distance

def lat_distance(lat, area):
    coords_1 = (lat, 0)
    coords_2 = (area.center_lat, 0)
    distance = geopy.distance.geodesic(coords_1, coords_2).m
    return distance

def calculate_r_azimuth(lat_distance, lon_distance):
    tangent = lon_distance/lat_distance
    azimuth = math.atan(tangent)
    r = math.sqrt(lat_distance ^ 2+lon_distance ^ 2)
    return (r, azimuth)

def correct_azimuth(area, p_r, p_azimuth):
    azimuth = p_azimuth+area.azimuth
    return (p_r, azimuth)

def to_lat_lon_distance(r, azimuth):
    lat = r*math.cos(azimuth)
    lon = r*math.sin(azimuth)
    return (lat, lon)

def calculate_in_rectangle(lat, lon, area: Area) -> bool:
    r, azimuth = calculate_r_azimuth(
        lat_distance(lat, area), lon_distance(lon, area))
    r, azimuth = correct_azimuth(r, azimuth)
    lat, lon = to_lat_lon_distance(r, azimuth)
    d_a = 1-(lat/area.a) ^ 2
    d_b = 1-(lon/area.b) ^ 2
    result = min(d_a, d_b)
    if result >= 0:
        return True
    return False

def position_in_area(lat: float, lon: float, area:Area) -> bool:
    if area.area_type == "circle":
        nlat = lat/10**7
        nlon = lon/10**7
        coords_1 = (nlat, nlon)
        coords_2 = (area.center_lat, area.center_lon)
        distance = geopy.distance.geodesic(coords_1, coords_2).m
        if distance < area.a:
            return True
        elif area.area_type == "rectangle":
            return calculate_in_rectangle(lat, lon, area)
    return False
