import flask
from flask import Flask
from flask import request, jsonify
from ldm import LDM
from prometheus_client import generate_latest


class AwarenessAPI:
    def __init__(self, ldm: LDM):
        self.ldm = ldm
        self.app = flask.Flask(__name__)
        self.app.config["DEBUG"] = False
        self.app.add_url_rule('/position/<id>', view_func=self.specific_position)
        self.app.add_url_rule('/positions', view_func=self.positions)
        self.app.add_url_rule('/metrics', view_func=self.metrics)

    def start(self):
        self.app.run(host="0.0.0.0", port=5000)

    # @self.app.route('/position/<id>', methods=['GET'])
    def specific_position(self, id):
        cam_position = self.ldm.get_position(id)
        latitude = cam_position.camParameters.basicContainer.referencePosition.latitude.value / \
            (10**7)
        longitude = cam_position.camParameters.basicContainer.referencePosition.longitude.value / \
            (10**7)
        position = {
            "latitude": latitude,
            "longitude": longitude
        }
        response = flask.jsonify(position)
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

    # @self.app.route('/positions', methods=['GET'])
    def positions(self):
        to_return = list()
        cam_positions = self.ldm.get_positions()
        for elem in cam_positions:
            latitude = elem.camParameters.basicContainer.referencePosition.latitude.value / \
                (10**7)
            longitude = elem.camParameters.basicContainer.referencePosition.longitude.value / \
                (10**7)
            to_return.append(
                {
                    "latitude": latitude,
                    "longitude": longitude
                }
            )
        response = flask.jsonify(to_return)
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    
    def metrics(self):
        response = flask.make_response(generate_latest())
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
