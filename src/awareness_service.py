from ldm import LDM
from area import Area
import v2xlib.v2xcom as v2xcom
import time
from v2xlib.messages.denm import DENM
from v2xlib.messages.its_container import SubCauseCodeType
from v2xlib.utils.encoder import Encoder
from utils import position_in_area
from instances import Instances, Sockets


class AwarenessService:
    def __init__(self, tram_station_id: int, ldm: LDM, area_critical: Area, area_warning:Area,broker_host: str, broker_port: int, instances: Instances):
        super().__init__()
        self.ldm = ldm
        self.tram_station_id = tram_station_id
        self.area_critical = area_critical
        self.area_warning = area_warning
        self.encoder = Encoder()
        self.instances = instances
        self.socket_config_warning = v2xcom.config.SocketConfig()
        self.socket_config_warning.set_btp_port(2002)
        self.socket_config_warning.set_general_name("WARNING")
        self.socket_config_warning.set_general_type("denm")
        self.socket_config_warning.set_general_timeout(600000000)
        self.socket_warning = self.instances.open_socket(
            self.socket_config_warning, self.on_receive)
        self.socket_config_critical = v2xcom.config.SocketConfig()
        self.socket_config_critical.set_btp_port(2002)
        self.socket_config_critical.set_general_name("CRITICAL")
        self.socket_config_critical.set_general_type("denm")
        self.socket_config_critical.set_general_timeout(600000000)
        self.socket_critical = self.instances.open_socket(
            self.socket_config_critical, self.on_receive)
        self.denm = DENM.from_xml("""<DENM>
    <header>
        <protocolVersion>2</protocolVersion>
        <messageID>1</messageID>
        <stationID>"""+str(self.tram_station_id)+"""</stationID>
    </header>
    <denm>
        <management>
            <actionID>
                <originatingStationID>1</originatingStationID>
                <sequenceNumber>12</sequenceNumber>
            </actionID>
            <detectionTime>0</detectionTime>
            <referenceTime>0</referenceTime>
            <eventPosition>
                <latitude>900000001</latitude>
                <longitude>1800000001</longitude>
                <positionConfidenceEllipse>
                    <semiMajorConfidence>4095</semiMajorConfidence>
                    <semiMinorConfidence>4095</semiMinorConfidence>
                    <semiMajorOrientation>3601</semiMajorOrientation>
                </positionConfidenceEllipse>
                <altitude>
                    <altitudeValue>800001</altitudeValue>
                    <altitudeConfidence><unavailable/></altitudeConfidence>
                </altitude>
            </eventPosition>
            <validityDuration>10</validityDuration>
            <stationType>11</stationType>
        </management>
        <situation>
            <informationQuality>0</informationQuality>
            <eventType>
                <causeCode>12</causeCode>
                <subCauseCode>1</subCauseCode>
            </eventType>
        </situation>
    </denm>
</DENM>""")
        


    def start(self):
        while True:
            coop_tram = self.ldm.get_position(self.tram_station_id)
            if coop_tram is not None:
                tram_latitude = coop_tram.camParameters.basicContainer.referencePosition.latitude.__dict__()
                tram_longitude = coop_tram.camParameters.basicContainer.referencePosition.longitude.__dict__()
                if position_in_area(tram_latitude, tram_longitude, self.area_critical):
                    self.ldm.set_tram_in_critical()
                    self.denm.denm.management.eventPosition = coop_tram.camParameters.basicContainer.referencePosition
                    self.denm.denm.situation.eventType.subCauseCode = SubCauseCodeType(1)
                    self.socket_critical.send(self.encoder.encode_denm(self.denm))
                elif position_in_area(tram_latitude, tram_longitude, self.area_warning):
                    self.ldm.set_tram_in_warning()
                if position_in_area(tram_latitude, tram_longitude, self.area_warning):
                    self.denm.denm.management.eventPosition = coop_tram.camParameters.basicContainer.referencePosition
                    self.denm.denm.situation.eventType.subCauseCode = SubCauseCodeType(2)
                    self.socket_warning.send(self.encoder.encode_denm(self.denm))
                else:
                    if self.ldm.tram_in_critical or self.ldm.tram_in_warning:
                        self.ldm.set_tram_out_area()
            time.sleep(0.3)

    def on_receive(self, message):
        pass
