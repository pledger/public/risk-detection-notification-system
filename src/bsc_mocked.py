import socket
import time
import threading

TRAMHARDCODED = [
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:00.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":270.0000,"speed":0.875,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:01.026Z","ept":0.005,"lat":41.407883333,"lon":2.204866667,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":110.6000,"speed":0.977,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:02.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":290.6000,"speed":0.514,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:03.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":90.0000,"speed":1.132,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:04.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":274.8000,"speed":2.006,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:05.026Z","ept":0.005,"lat":41.407883333,"lon":2.204866667,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":96.1000,"speed":1.595,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:06.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":336.1000,"speed":0.720,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:07.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":249.4000,"speed":0.514,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:08.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":146.4000,"speed":0.514,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:09.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":270.0000,"speed":0.669,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:10.026Z","ept":0.005,"lat":41.407883333,"lon":2.204866667,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":90.0000,"speed":0.669,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:11.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":270.0000,"speed":0.669,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:12.026Z","ept":0.005,"lat":41.407883333,"lon":2.204866667,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":233.1000,"speed":0.309,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:13.026Z","ept":0.005,"lat":41.407883333,"lon":2.204866667,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":69.4000,"speed":0.514,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:14.026Z","ept":0.005,"lat":41.407883333,"lon":2.204866667,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":53.1000,"speed":0.309,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:15.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":306.9000,"speed":0.617,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:16.026Z","ept":0.005,"lat":41.407883333,"lon":2.204866667,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":110.6000,"speed":0.514,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:17.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":156.1000,"speed":0.720,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:18.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":270.0000,"speed":0.463,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:19.026Z","ept":0.005,"lat":41.407883333,"lon":2.204866667,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":90.0000,"speed":0.206,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:20.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":306.9000,"speed":0.309,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:21.026Z","ept":0.005,"lat":41.407883333,"lon":2.204866667,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":110.6000,"speed":0.977,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:22.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":277.1000,"speed":1.338,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:23.026Z","ept":0.005,"lat":41.407883333,"lon":2.204866667,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":59.0000,"speed":1.440,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:24.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":167.5000,"speed":1.338,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:25.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":270.0000,"speed":0.669,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:26.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":0.0000,"speed":0.463,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:27.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":0.0000,"speed":0.206,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:28.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":180.0000,"speed":0.206,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:29.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":0.0000,"speed":0.206,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:30.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":263.9000,"speed":1.595,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:31.026Z","ept":0.005,"lat":41.407883333,"lon":2.204850000,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":64.8000,"speed":2.109,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:32.026Z","ept":0.005,"lat":41.407883333,"lon":2.204883333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":256.1000,"speed":15.022,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:33.026Z","ept":0.005,"lat":41.407850000,"lon":2.204700000,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":256.1000,"speed":15.022,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:34.026Z","ept":0.005,"lat":41.407800000,"lon":2.204533333,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":256.1000,"speed":15.022,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:35.026Z","ept":0.005,"lat":41.407766667,"lon":2.204366667,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":256.1000,"speed":15.022,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:36.026Z","ept":0.005,"lat":41.407716667,"lon":2.204200000,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":256.1000,"speed":15.022,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:37.026Z","ept":0.005,"lat":41.407683333,"lon":2.204016667,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":256.1000,"speed":15.022,"climb":0.000,"epc":46.00}',
    '{"class":"TPV","device":"/dev/pts/3","mode":3,"time":"2021-07-12T10:28:38.026Z","ept":0.005,"lat":41.407633333,"lon":2.203850000,"epx":8.723,"epy":10.589,"alt":0.000,"epv":23.000,"track":256.1000,"speed":15.022,"climb":0.000,"epc":46.00}'
]


class BSCServer:
    def __init__(self, host: str = '0.0.0.0', port: int = 2947):
        self.host = host 
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind(self.host, self.port)
        self.connections = list()
        self.index = 0
        self.sockets_thread = threading.Thread(target=self.start, daemon=True)
        self.positions_thread = threading.Thread(target=self.start_sending_positions, daemon=True)
        self.sockets_thread.start()
        self.positions_thread.start()
        self.sockets_thread.join()
        self.positions_thread.join()

    def start(self):
        self.socket.listen()
        conn, addr = s.accept()
        self.connections.append(conn)

    def send_position(self):
        mocked_message = [
            {
                "type" : "tram",
                "TPV" : json.loads(TRAMHARDCODED[self.index])
            }
        ]
        self.index = (self.index+1)%len(TRAMHARDCODED)
        for conn in self.connections:
            conn.sendall(json.dumps(mocked_message).encode('utf-8'))
    
    def start_sending_positions(self):
        while True:
            self.send_position()
            time.sleep(1)
        
        

if __name__ == "__main__":
