FROM python:3.8-alpine3.12

RUN apk update &&\
    apk upgrade &&\
    apk add bash &&\
    apk add bash-doc &&\
    apk add bash-completion
ADD v2xlib /v2xlib
RUN cd /v2xlib &&\
    python setup.py install &&\
    cd 
ADD src /src
RUN pip install -r src/requirements.txt
ADD v2xlib/v2xlib/utils/asn1 /usr/local/lib/python3.8/site-packages/v2xlib-0.0.1-py3.8.egg/v2xlib/utils/asn1
RUN pip install -r src/requirements.txt
ENTRYPOINT [ "python", "-u", "/src/awareness_app.py" ]
CMD [ "--station_id", "1"]